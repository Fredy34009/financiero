<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page session="true"%>
<%
	if (session.getAttribute("userLog") == null) {
		response.sendRedirect("index.html");
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style type="text/css">
body {
	background: #f9f9f9;
}

#wrapper {
	padding: 90px 15px;
}

.navbar-expand-lg .navbar-nav.side-nav {
	flex-direction: column;
}

.card {
	margin-bottom: 15px;
	border-radius: 0;
	box-shadow: 0 3px 5px rgba(0, 0, 0, .1);
}

.header-top {
	box-shadow: 0 3px 5px rgba(0, 0, 0, .1)
}

.leftmenutrigger, .navbar-nav li a .shortmenu {
	display: none
}

.card-title {
	font-size: 28px
}

@media ( min-width :992px) {
	.leftmenutrigger {
		display: block;
		display: block;
		margin: 7px 20px 4px 0;
		cursor: pointer;
	}
	#wrapper {
		padding: 90px 15px 15px 75px;
	}
	#wrapper.open {
		padding: 90px 15px 15px 225px;
	}
	.navbar-nav.side-nav.open {
		left: 0;
	}
	.side-nav li a {
		padding: 20px
	}
	.navbar-nav li a .shortmenu {
		float: right;
		display: block;
		opacity: 1
	}
	.navbar-nav.side-nav.open.navbar-nav li a .shortmenu {
		opacity: 0
	}
	.navbar-nav.side-nav {
		background: #585f66;
		box-shadow: 2px 1px 2px rgba(0, 0, 0, .1);
		position: fixed;
		top: 56px;
		flex-direction: column !important;
		left: -140px;
		width: 200px;
		overflow-y: auto;
		bottom: 0;
		overflow-x: hidden;
		padding-bottom: 40px
	}
}

.animate {
	-webkit-transition: all .2s ease-in-out;
	-moz-transition: all .2s ease-in-out;
	-o-transition: all .2s ease-in-out;
	-ms-transition: all .2s ease-in-out;
	transition: all .2s ease-in-out
}

.navbar-nav li a svg {
	font-size: 25px;
	float: left;
	margin: 0 10px 0 5px;
}

.side-nav li {
	border-bottom: 1px solid #50575d;
}
</style>
</head>
<body>
	<nav
		class="navbar header-top fixed-top navbar-expand-lg navbar-dark bg-dark">
		<span class="navbar-toggler-icon leftmenutrigger"></span> <a
			class="navbar-brand" href="actividad">inicio</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarText" aria-controls="navbarText"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarText">
			<ul class="navbar-nav animate side-nav">
				<c:choose>
					<c:when test="${userLog.tipoUsuarios.id==1}">
						<li class="nav-item"><a class="nav-link" href="fechaespecial"
							title="Asutos"><i class="fa fa-calendar" aria-hidden="true"></i>Asuetos<i
								aria-hidden="true" class="fa fa-calendar shortmenu animate"></i></a>
						</li>
						<li class="nav-item"><a class="nav-link" href="actividad"
							title="Actividades"><i class="fa fa-book"></i>Actividades<i
								class="fa fa-book shortmenu animate"></i></a></li>
						<li class="nav-item"><a class="nav-link" href="newPersona"
							title="Usuarios"><i class="fas fa-user"></i>Usuarios<i
								class="fas fa-user shortmenu animate"></i></a></li>
						<li class="nav-item"><a class="nav-link" href="reportes"
							title="Reportes"><i class="fas fa-file "></i>Reportes<i
								class="fas fa-file  shortmenu animate"></i></a></li>
					</c:when>
				</c:choose>
			</ul>
			<ul class="navbar-nav ml-md-auto d-md-flex">
				<li class="nav-item"><a class="nav-link" href="close"><i
						class="fas fa-key"></i> Salir ${userLog.usuario}</a></li>
			</ul>
		</div>
	</nav>
	<script defer
		src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.leftmenutrigger').on('click', function(e) {
				$('.side-nav').toggleClass("open");
				$('#wrapper').toggleClass("open");
				e.preventDefault();
			});
		});
	</script>
</body>
</html>