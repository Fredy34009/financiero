<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page session="true"%>
<%
	if (session.getAttribute("userLog") == null) {
		response.sendRedirect("index.html");
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Asuetos</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<div id="wrapper" class="animate">
		<jsp:include page="menu.jsp" />
		<div class="container-fluid">
			<div class="row">
				<div class="col-4">
					<div class="card">
						<div class="card-body">
							<h5>
								A�o Actual : <i style="color: green;">${annio}</i>
							</h5>
							<h5 class="card-title">Dias de Asueto</h5>
							<form action="fechaespecial" method="post"  onsubmit="return enviado();"
								>

								<c:choose>
									<c:when test="${dia.fecha==null}">
										<label>Dia</label>
										<input type="date" name="fecha" pattern="yyyy-MM-dd">
									</c:when>
									<c:otherwise>
										<label>Dia</label>
										<input type="date"
											value="<fmt:formatDate pattern="yyyy-MM-dd"
													value="${dia.fecha}" />"
											name="fecha">
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${dia.fecha==null}">
										<button name="b" value="1"  class="btn btn-success">Guardar</button>
									</c:when>
									<c:otherwise>
										<button name="b" value="2" class="btn btn-success">Actualizar</button>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${dia.fecha==null}">
										<input name="id" value="0" class="btn btn-success"
											type="hidden"/>
									</c:when>
									<c:otherwise>
										<input name="id" value="${dia.id}" type="hidden" />
									</c:otherwise>
								</c:choose>
							</form>
						</div>
					</div>
				</div>
				<div class="col-8">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Dias de Asueto</h5>
							<table class="table">
								<thead>
									<tr>
										<th>Fecha</th>
										<th>Accion</th>
									<tr>
								</thead>
								<tbody>
									<c:forEach items="${dias}" var="d">
										<tr>
											<td><fmt:formatDate pattern="dd-MM-yyyy"
													value="${d.fecha}" /></td>
											<td><a href="fecharem?id=${d.id}"><i
													class="fa fa-trash-alt" style="font-size: 36px"></i></a></td>
										<tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script LANGUAGE="JavaScript">
		var cuenta = 0;

		function enviado() {
			if (cuenta == 0) {
				cuenta++;
				return true;
			} else {
				alert("El siguiente formulario ya ha sido enviado, muchas gracias.");
				return false;
			}
		}
	</script>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>
</body>
</html>