<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page session="true"%>
<%
	if (session.getAttribute("userLog") == null) {
		response.sendRedirect("index.html");
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<div id="wrapper" class="animate">
		<jsp:include page="menu.jsp" />
		<div class="container-fluid">
			<div class="row">
				<div class="col-9">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Actividades</h5>
							<table class="table table-striped table-bordered">
								<thead>
									<tr>
										<td>Actividad</td>
										<td>Asignada</td>
										<td>Fecha finalizacion</td>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${actividad}" var="act">
										<tr>
											<td>${act.nombre}</td>
											<td>${act.usuarios.personas.nombre}</td>
											<td><fmt:formatDate pattern="yyyy-MM-dd"
													value="${act.fecha}" /> &nbsp; <c:choose>
													<c:when test="${userLog.tipoUsuarios.id==1}">
														<a href="repActividad?id=${act.id}" title="Reprogramar"><i
															class="fa fa-clock" style="font-size: 20px"></i></a>&nbsp;<a
															href="remActividad?id=${act.id}" title="Eliminar"><i
															class="fa fa-trash-alt" style="font-size: 20px"></i></a>
													</c:when>
													<c:otherwise>
														<form action="stateOperario" method="post">
															<input name="act" type="hidden" value="${act.id}" /> <label>Cambiar
																Fase</label> <select name="fas">
																<c:forEach items="${fases}" var="fase">
																	<option value="${fase.id}">${fase.fase}</option>
																</c:forEach>
															</select>
															<button>
																<i class="fas fa-calendar-check" aria-hidden="true"></i>
															</button>
														</form>
													</c:otherwise>
												</c:choose></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>
</body>
</html>