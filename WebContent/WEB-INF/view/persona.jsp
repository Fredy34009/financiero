<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page session="true"%>
<%
	if(session.getAttribute("userLog") == null)
	{
		response.sendRedirect("index.html");
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Usuarios</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<div id="wrapper" class="animate">
		<jsp:include page="menu.jsp" />
		<div class="container">
			<div class="row">
				<div class="col-4">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Registro Usuario</h5>
							<form action="newPersona" method="post">
								<label>Nombre</label> <input class="form-control" name="name" />
								<label>Correo</label> <input class="form-control" name="correo" />
								<label>Tipo de usuario</label> <select name="tipou"
									class="form-control">
									<c:forEach items="${tiposUser}" var="tipo">
										<option value="${tipo.id}">${tipo.tipo}</option>
									</c:forEach>
								</select> <br /> <label>Usuario</label> <input class="form-control"
									name="user" /> <label>Contraseņa</label> <input
									class="form-control" type="password" name="pass" /> <br />
								<button class="btn btn-success">Registrar</button>
							</form>
							${st}
						</div>
					</div>
				</div>
				<div class="col-8">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Usuarios</h5>
							<table class="table-hover table">
								<thead>
									<tr>
										<td>Usuario</td>
										<td>Persona</td>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${usuarios}" var="us">
										<tr>
											<td>${us.usuario}</td>
											<td>${us.personas.nombre}</td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>
</body>
</html>