package com.financiero.control;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.financiero.models.Usuarios;
import com.financiero.util.Dao;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Controller
public class ReportesManager {

	@Autowired
	Dao<Usuarios> usuariosImp;
	@Autowired
	ServletContext servletContext;
	@Autowired
	ServletResponse response;

	//Crea el reporte dependiendo del dato pasado
	@RequestMapping(value = "/repor", method = RequestMethod.GET)
	public void repor(@RequestParam("id")int id) throws JRException, IOException {
		
			List<Usuarios> lista=usuariosImp.read();
			if(id==1)
			{
				System.out.println("Lista "+lista.size());
				JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(lista);
				File reportFile = new File(servletContext.getRealPath("/reportes/usuario.jasper"));

				byte[] bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), null, ds);
				response.setContentType("application/pdf");
				response.setContentLength(bytes.length);
				ServletOutputStream ouputStream = response.getOutputStream();
				ouputStream.write(bytes, 0, bytes.length);
				ouputStream.flush();
				ouputStream.close();
				
			}
			else
			{
				JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(usuariosImp.read());
				File reportFile = new File(servletContext.getRealPath("/reportes/usuario.jasper"));

				byte[] bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), null, ds);
				response.setContentType("application/pdf");
				response.setContentLength(bytes.length);
				ServletOutputStream ouputStream = response.getOutputStream();
				ouputStream.write(bytes, 0, bytes.length);
				ouputStream.flush();
				ouputStream.close();
			}
			return;
	}
	@RequestMapping(value = "/reportes",method = RequestMethod.GET)
	public ModelAndView reportes(ModelAndView mav)
	{
		mav=new ModelAndView();
		mav.setViewName("reportes");
		return mav;
	}
}
