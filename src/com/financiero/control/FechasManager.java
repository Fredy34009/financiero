package com.financiero.control;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.financiero.models.FechasEspeciales;
import com.financiero.util.Dao;

@Controller
public class FechasManager {

	@Autowired
	Dao<FechasEspeciales> feDao;
	

	ModelAndView mav = new ModelAndView();

	//Metodo que carga la vista de ferchas especiales
	@RequestMapping(value = "/fechaespecial")
	public ModelAndView fechaespecial() {
		
		Calendar cal=Calendar.getInstance();
		int annio=cal.get(Calendar.YEAR);
		
		mav.addObject("dias", feDao.read());
		mav.addObject("annio", annio);
		mav.setViewName("asuetos");
		return mav;
	}

	//metodo para guardar y editar fechas especiales
	@RequestMapping(value = "/fechaespecial", method = RequestMethod.POST)
	public ModelAndView fechaespecial(@RequestParam("fecha") String fech,@RequestParam("b") int b,@RequestParam("id") int id) throws ParseException {
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		Date fecha = formato.parse(fech);
		System.out.println("fecha "+fecha);

		FechasEspeciales especiales=new FechasEspeciales();
		if(b==1)
		{
			especiales.setFecha(fecha);
			feDao.create(especiales);
		}
		else if(b==2)
		{
			especiales.setId(id);
			especiales.setFecha(fecha);
			feDao.edit(especiales);
			
		}
		especiales=new FechasEspeciales();
		mav.addObject("dia",null);
		mav.setViewName("asuetos");
		return fechaespecial();
	}
	
	@RequestMapping(value = "/fechaedit", method = RequestMethod.GET)
	public ModelAndView fechaedit(@RequestParam("dia") int dia) throws ParseException {

		FechasEspeciales especiales=new FechasEspeciales();
		especiales.setId(dia);
		mav.addObject("dia",feDao.readById(dia));
		mav.setViewName("asuetos");
		return fechaespecial();
	}
	//Metodo remover fecha especial
	@RequestMapping(value = "/fecharem", method = RequestMethod.GET)
	public ModelAndView fecharem(@RequestParam("id") int id){

		FechasEspeciales especiales=new FechasEspeciales();
		especiales.setId(id);
		feDao.delete(especiales);
		mav.setViewName("asuetos");
		return fechaespecial();
	}
}
