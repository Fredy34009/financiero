package com.financiero.util;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import com.financiero.models.Actividades;
import com.financiero.models.Estados;
import com.financiero.models.FaseDetalles;
import com.financiero.models.Fases;
import com.financiero.models.FechasEspeciales;
import com.financiero.models.Personas;
import com.financiero.models.TipoUsuarios;
import com.financiero.models.Usuarios;

public class HibernateUtil {

	private static SessionFactory sessionFactory;

	public static SessionFactory factory() {
		try {
			if (sessionFactory == null) {
				Configuration conf=new Configuration();
				Properties settings = new Properties();
				settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
				settings.put(Environment.URL, "jdbc:mysql://localhost:3306/financiero?useSSL=false");
				settings.put(Environment.USER, "root");
				settings.put(Environment.PASS, "root");
				settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
				settings.put(Environment.SHOW_SQL, "true");
				settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
				conf.setProperties(settings);
				conf.addAnnotatedClass(Actividades.class);
				conf.addAnnotatedClass(Estados.class);
				conf.addAnnotatedClass(Fases.class);
				conf.addAnnotatedClass(FaseDetalles.class);
				conf.addAnnotatedClass(FechasEspeciales.class);
				conf.addAnnotatedClass(Personas.class);
				conf.addAnnotatedClass(TipoUsuarios.class);
				conf.addAnnotatedClass(Usuarios.class);
				ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(conf.getProperties()).build();
				sessionFactory = conf.buildSessionFactory(serviceRegistry);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return sessionFactory;
	}
}
