package com.financiero.util;

import java.util.List;
import org.hibernate.Session;

public abstract class AbstractFacade<T> {

	private Session session=HibernateUtil.factory().openSession();

	private Class<T> entity;
	
	public AbstractFacade(Class<T> entity) {
		super();
		this.entity = entity;
	}

	public void create(T entity) {
		try {
			session.beginTransaction();
			session.persist(entity);
			session.getTransaction().commit();
		} catch (Exception e) {
		}
	}
	public void edit(T entity) {
		try {
			session.beginTransaction();
			session.merge(entity);
			session.getTransaction().commit();
		} catch (Exception e) {
		}
	}
	public T readById(int id) {
		session.beginTransaction();
		T t = session.get(entity, id);
		session.flush();
		session.getTransaction().commit();
		return t;
	}
	public void delete(T entity) {
		session.beginTransaction();
		session.delete(session.merge(entity));
		session.flush();
		session.getTransaction().commit();
	}
	@SuppressWarnings("unchecked")
	public List<T> read() {
		try {
			session.beginTransaction();
			List<T> list = session.createQuery("FROM " + entity.getName()).list();
			session.flush();
			session.getTransaction().commit();
			return list;
		} catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		
	}

}
