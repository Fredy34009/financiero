package com.financiero.util;

import java.util.List;

public interface Dao<T> {

	public void create(T e);
	public void edit(T e);
	public List<T> read();
	public T readById(int id);
	public void delete(T entity);

}
