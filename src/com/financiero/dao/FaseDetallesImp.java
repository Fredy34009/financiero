package com.financiero.dao;

import org.springframework.stereotype.Repository;

import com.financiero.models.FaseDetalles;
import com.financiero.util.AbstractFacade;
import com.financiero.util.Dao;

@Repository
public class FaseDetallesImp extends AbstractFacade<FaseDetalles> implements Dao<FaseDetalles> {

	
	public FaseDetallesImp() {
		super(FaseDetalles.class);
	}

}
