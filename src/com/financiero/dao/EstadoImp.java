package com.financiero.dao;

import org.springframework.stereotype.Repository;

import com.financiero.models.Estados;
import com.financiero.util.AbstractFacade;
import com.financiero.util.Dao;

@Repository
public class EstadoImp extends AbstractFacade<Estados> implements Dao<Estados> {

	
	public EstadoImp() {
		super(Estados.class);
	}

}
