package com.financiero.dao;

import com.financiero.models.Fases;
import com.financiero.util.AbstractFacade;
import com.financiero.util.Dao;

public class FasesImp extends AbstractFacade<Fases> implements Dao<Fases> {

	public FasesImp() {
		super(Fases.class);
	}

}
