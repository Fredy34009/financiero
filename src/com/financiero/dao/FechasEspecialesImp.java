package com.financiero.dao;

import com.financiero.models.FechasEspeciales;
import com.financiero.util.AbstractFacade;
import com.financiero.util.Dao;

public class FechasEspecialesImp extends AbstractFacade<FechasEspeciales> implements Dao<FechasEspeciales> {

	public FechasEspecialesImp() {
		super(FechasEspeciales.class);
	}

}
