package com.financiero.dao;

import com.financiero.models.Personas;
import com.financiero.util.AbstractFacade;
import com.financiero.util.Dao;

public class PersonasImp extends AbstractFacade<Personas> implements Dao<Personas> {

	public PersonasImp() {
		super(Personas.class);
	}

}
