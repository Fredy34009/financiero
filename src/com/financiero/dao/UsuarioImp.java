package com.financiero.dao;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.financiero.models.Usuarios;
import com.financiero.util.AbstractFacade;
import com.financiero.util.Dao;
import com.financiero.util.HibernateUtil;

@Repository
public class UsuarioImp extends AbstractFacade<Usuarios> implements Dao<Usuarios> {

	Session session=HibernateUtil.factory().openSession();
	
	public UsuarioImp() {
		super(Usuarios.class);
	}
	public Usuarios login(Usuarios us)
	{
		try {
			CriteriaBuilder cb=session.getCriteriaBuilder();
			CriteriaQuery<Usuarios> cq=cb.createQuery(Usuarios.class);
			Root<Usuarios> root=cq.from(Usuarios.class);
			
			cq.where(cb.equal(root.get("usuario"), us.getUsuario()),cb.and(cb.equal(root.get("pass"),us.getPass())));
			Query q=(Query) session.createQuery(cq);
			us=(Usuarios) q.getSingleResult();
			System.out.println("Usuario "+us.getUsuario());
			return us;
		} catch (Exception e) {
			return null;
		}
	}

}
